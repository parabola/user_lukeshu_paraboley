import os
from setuptools import setup

def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()

setup(
    name="Paraboley",
    version="0.3",
    author="Aurélien DESBRIÈRES",
    author_email="aurelien@hackers.camp",
    description="A simple python script to display a Parabola GNU/Linux-libre logo in ASCII art along with basic system information.",
    license="GPL",
    url="https://git.parabola.nu/packages/paraboley.git",
    long_description=read("README.md"),
    scripts=["paraboley"]
)
